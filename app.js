// defino variable express para instanciar la clase express y crear servidor web
var express = require('express');

// instancio la clase express en el objeto app
var app = express();

// defino la variable bodyParser para instanciar la clase body-parser
var bodyParser = require('body-parser');

// uso la bliblioteca bodyparser para parsear el body
app.use(bodyParser.json());

// de esta forma puedo parametrizar el puerto a utilizar y sino hay ninguno por defecto utiliza el 3000
var port = process.env.PORT || 5000;

// creo una variable para el prefijo de la uri
const URI= '/api-uruguay/v1/';

// creo una variable para manejar el archivo de users
var usersFile = require('./users.json');

// creo una variable para manejar el archivo de accounts
var accountsFile = require('./accounts.json');

app.listen(port);
console.log('Escuchando en el puerto 3000...');

// implemento el GET de users
app.get(URI + 'users',
    function(req, res){
      console.log('GET /users');
      res.send(usersFile);
    });

// implemento el GET de users con ID
app.get(URI + 'users/:id',
    function(peticion, respuesta){
      console.log ('GET /users/id ' + peticion.params.id);
      let idUser = peticion.params.id;
      respuesta.send(usersFile[idUser - 1]);
    });

// implemento el GET de accounts
app.get(URI + 'accounts',
    function(req, res){
      console.log('GET /accounts');
      res.send(accountsFile);
    });

// implemento el GET de accounts con ID
app.get(URI + 'accounts/:id',
    function(peticion, respuesta){
      console.log ('GET /accounts/id ' + peticion.params.id);
      let idAccount = peticion.params.id;
      respuesta.send(accountsFile[idAccount - 1]);
    });

//POST users en el body
app.post(URI + 'users',
    function(req, res){
      console.log('POST /users');
      console.log(req.body);
      let tam = usersFile.length + 1;
      var userNuevo = {
        'id': tam,
        'first_name': req.body.first_name,
        'last_name':req.body.last_name,
        'email': req.body.email,
        'password':req.body.password
      };
      usersFile.push(userNuevo);
      res.send({'message':'Nuevo usuario añadido correctamente', userNuevo});
});

//POST users en el  header
app.post(URI + 'users_post',
    function(req, res){
      console.log('POST /users en el header');
      console.log(req.headers);
      let tam = usersFile.length + 1;
      var userNuevo = {
        'id': tam,
        'first_name': req.headers.first_name,
        'last_name':req.headers.last_name,
        'email': req.headers.email,
        'password':req.headers.password
      };
      usersFile.push(userNuevo);
      res.send({'message':'Header nuevo usuario añadido', userNuevo});
});

//PUT users
app.put(URI + 'users/:id',
  function(req, res){
    console.log('PUT users');
    let idUpdate = req.params.id;
    var existe = true;
    if (usersFile[idUpdate-1] == undefined) {
      console.log("Usuario NO existe");
      existe = false;
    } else {
      let userNuevo = {
        'id': idUpdate,
        'first_name': req.body.first_name,
        'last_name':req.body.last_name,
        'email': req.body.email,
        'password':req.body.password
      }
      usersFile[idUpdate - 1] = userNuevo;
    }
    var msgResponse = existe ? {'msg':'Update Ok'} : {'msg':'Update ha fallado'};
    var statusCode = existe ? 200 : 400;
    res.status(statusCode).send(msgResponse);
  });

  //DELETE users
  app.delete(URI + 'users/:id',
    function(req, res){
      console.log('DELETE users');
      let idUser = req.params.id;
      var existe = true;
      if (usersFile[idUser-1] == undefined) {
        console.log("Usuario NO existe");
        existe = false;
      } else {
          usersFile.splice(idUser-1,1);
      }
      var msgResponse = existe ? {'msg':'Delete Ok'} : {'msg':'Delete ha fallado'};
      var statusCode = existe ? 200 : 400;
      res.status(statusCode).send(msgResponse);
    });

// GET con query string ?qname='Gustavo'&qlast_name='Morelis'"
app.get(URI + "user",
    function(req, res){
      console.log('GET con query string');
      let msgResponse = {'msg':'Usuario NO encontrado'};
      let encontrado = false;

//      console.log(req.query.qname);
//      console.log(req.query.qlast_name);

      for(i = 0 ; i < usersFile.length && !encontrado ; ++i){
//        console.log(usersFile[i].first_name);
//        console.log(usersFile[i].last_name);
        if (usersFile[i].first_name == req.query.qname && usersFile[i].last_name == req.query.qlast_name){
           msgResponse = {'msg':'Usuario encontrado: ' + usersFile[i].first_name +' '+ usersFile[i].last_name};
           encontrado = true;
        }
  //      else {
  //          let msgResponse = {'msg':'Usuario NO encontrado'};
  //      }
      }
    res.send(msgResponse);

    });
