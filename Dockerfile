# Imagen docker base
FROM node:latest

# Defino el directorio de trabajo
WORKDIR /docker-api

# Copiar archivos del proyecto al directorio
ADD . /docker-api

#queda comentado porque ya lo tenemos, pero tiene que ir el npm install para instalar las dependencias del proyecto
#RUN npm install

# Puerto donde exponemos contenedor
EXPOSE 3001

#Comando para lanzar la app
CMD ["npm", "start"]
