// defino variable express para instanciar la clase express y crear servidor web
var express = require('express');

// instancio la clase express en el objeto app
var app = express();

// importo el módulo config.js
let config = require('./config');

// instancio variables/constantes del config*********
// URI Base
const URI = config.URI;

// url Mlab
var baseMLabURL = config.baseMLabURL;

// almaceno la apikey de MLab
var apiKeyMLab = config.apiKeyMLab;
//***************************************************

// defino la variable bodyParser para instanciar la clase body-parser
var bodyParser = require('body-parser');

// uso la bliblioteca bodyparser para parsear el body
app.use(bodyParser.json());

// de esta forma puedo parametrizar el puerto a utilizar y sino hay ninguno por defecto utiliza el 3000
var port = process.env.PORT || 3001;

// creo una variable para manejar el archivo de users
//var usersFile = require('./users.json');

// creo una variable para manejar el archivo de accounts
//var accountsFile = require('./accounts.json');

// creo una variable para usar la dependencia request-json
var requestJSON = require('request-json');

// escucho en el puerto seleccionado
app.listen(port);
console.log('Escuchando cuentas en el puerto 3001...');


// Petición GET de todas las cuentas de un usuario con id en mLab
app.get(URI + 'users/:id/cuentas',
  function (req, res) {
    console.log("GET" + URI + "users/:id/cuentas");
    console.log(req.params.id);
    var id = req.params.id;
    var queryString = 'q={"id":' + id + '}/cuentas&';

    var clienteMlab = requestJSON.createClient(baseMLabURL);
    clienteMlab.get('ucm?' + queryString + apiKeyMLab,
      function(err, respuestaMLab, body){
        console.log("Respuesta mLab correcta.");

        if(err) {
              res.status(500).send({"msg" : "Error obteniendo cuentas."});
        } else {
          if(body.length > 0) {
              res.status(200).send(body[0].cuentas);
          } else {
              res.status(404).send({"msg" : "Usuario no encontrado."});

          }
        }
      });
});

// Petición GET de una cuenta de un usuario con id en mLab
app.get(URI + 'users/:id/cuentas/:idCuenta',
  function (req, res) {
    console.log("GET" + URI + "users/:id/cuentas/:idCuenta");
    console.log(req.params.id);
    console.log(req.params.idCuenta);
    var id = req.params.id;
    var idCuenta = req.params.idCuenta;
    var queryString = 'q={"id":' + id + '}&';

    var clienteMlab = requestJSON.createClient(baseMLabURL);

    console.log(baseMLabURL + "ucm?" + queryString + apiKeyMLab);

    clienteMlab.get('ucm?' + queryString + apiKeyMLab,
      function(err, respuestaMLab, body){
        console.log("Respuesta mLab correcta.");

        if(err) {
              res.status(500).send({"msg" : "Error obteniendo cuentas."});
        } else {
          if(body.length > 0) {

              console.log(body[0]);
              if(body[0].cuentas.length > 0) {
                  var existecuenta = false;
                  for(var i = 0; i < body[0].cuentas.length; i++){
                    if(body[0].cuentas[i].idCuenta == idCuenta) {
                      var existecuenta = true;
                      console.log(body[0].cuentas[i].idCuenta);
                      console.log(body[0].cuentas[i].iban);
                      console.log(body[0].cuentas[i].moneda);
                      console.log(body[0].cuentas[i].saldo);
                      res.status(200).send(body[0].cuentas[i]);
                    }
                  }
                  if (!existecuenta) {
                    res.status(404).send({"msg" : "Cuenta no encontrada."});
                  }
              } else {
                  res.status(404).send({"msg" : "Cuenta no encontrada."});
              }

          } else {
              res.status(404).send({"msg" : "Usuario no encontrado."});

          }
        }
      });
});

// Petición PUT para acreditar/debitar una cuenta con id en mLab
app.put(URI + 'users/:id/cuentas/:idCuenta',
  function (req, res) {
//    console.log(req.params.id);
//    console.log(req.params.idCuenta);
//    console.log(req.body.importe);
    var id = req.params.id;
    var idCuenta = req.params.idCuenta;
    var importe = req.body.importe;
    var asunto = req.body.asunto;

    var queryString = 'q={"id":' + id + '}&';

    var clienteMlab = requestJSON.createClient(baseMLabURL);

    console.log(baseMLabURL + "ucm?" + queryString + apiKeyMLab);

    clienteMlab.get('ucm?' + queryString + apiKeyMLab,
      function(err, respuestaMLab, body){
        console.log("Respuesta mLab correcta.");

        if(err) {
              res.status(500).send({"msg" : "Error obteniendo cuentas."});
        } else {
          if(body.length > 0) {

              if(body[0].cuentas.length > 0) {
                  var existecuenta = false;
                  for(var i = 0; i < body[0].cuentas.length; i++){
                    if(body[0].cuentas[i].idCuenta == idCuenta) {
                      var existecuenta = true;
                      body[0].cuentas[i].saldo = body[0].cuentas[i].saldo + importe;

                      Cuenta = body[0];

                      if(Cuenta.cuentas[i].saldo>=0) {
                          var cont=i;

                          // Obtengo último movimiento de la cuenta
                          var idMovimiento=0;
                          for(var x = 0; x < Cuenta.cuentas[cont].movimientos.length; x++){
                            idMovimiento = Cuenta.cuentas[cont].movimientos[x].idMov;
                          }
    //                      console.log("idMovimiento:" + idMovimiento);

                          idMovimiento = idMovimiento + 1;

                          //Obtengo fecha actual
                          var dt = new Date();
                          var month = dt.getMonth()+1;
                          var day = dt.getDate();
                          var year = dt.getFullYear();
                          var fecha = (day + '/' + month + '/' + year);

                          //Armo movimiento a insertar
                          var mov = {
                                    "idMov": +idMovimiento,
                                    "fecha": fecha,
                                    "importe": importe,
                                    "asunto": asunto
                                }

                          console.log(mov);
                          Cuenta.cuentas[cont].movimientos[x] = mov;
                          console.log(Cuenta.cuentas[cont].movimientos);

                          clienteMlab.put('ucm?' + queryString + apiKeyMLab, Cuenta,
                            function(errp, respuestaMLabp, bodyp){
                              if(errp) {
                                    res.status(500).send({"msg" : "Error actualizando saldo"});
                              } else {
                                  res.status(200).send(Cuenta);
                              }
                            });
                      } else {
                            res.status(404).send({"msg" : "Saldo insuficiente."});
                      }
                  }
                  }
                  if (!existecuenta) {
                    res.status(404).send({"msg" : "Cuenta no encontrada."});
                  }
              } else {
                  res.status(404).send({"msg" : "Cuenta no encontrada."});
              }
          } else {
              res.status(404).send({"msg" : "Usuario no encontrado."});

          }
        }
      });
});
