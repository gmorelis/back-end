//Primer versión de Usuarios con EXPORTACIONES a partir del 16/10

// defino variable express para instanciar la clase express y crear servidor web
var express = require('express');

// instancio la clase express en el objeto app
var app = express();

// importo el módulo config.js
let config = require('./config');

// instancio variables/constantes del config*********
// URI Base
const URI = config.URI;

// url Mlab
var baseMLabURL = config.baseMLabURL;

// almaceno la apikey de MLab
var apiKeyMLab = config.apiKeyMLab;
//***************************************************

// defino la variable bodyParser para instanciar la clase body-parser
var bodyParser = require('body-parser');

// uso la bliblioteca bodyparser para parsear el body
app.use(bodyParser.json());

// de esta forma puedo parametrizar el puerto a utilizar y sino hay ninguno por defecto utiliza el 3000
var port = process.env.PORT || 3000;

// creo una variable para manejar el archivo de users
//var usersFile = require('./users.json');

// creo una variable para manejar el archivo de accounts
//var accountsFile = require('./accounts.json');

// creo una variable para usar la dependencia request-json
var requestJSON = require('request-json');

// escucho en el puerto seleccionado
app.listen(port);
console.log('Escuchando usuarios en el puerto 3000...');


// GET users consumiendo API REST de mLab
app.get(URI + 'users',
  function(req, res) {
    console.log("GET" + URI + "users");
    var httpClient = requestJSON.createClient(baseMLabURL);
    console.log("Cliente HTTP mLab creado.");
    var queryString = 'f={"_id":0}&';
    httpClient.get('ucm?' + queryString + apiKeyMLab,
      function(err, respuestaMLab, body) {
        var response = {};
        if(err) {
            response = {
              "msg" : "Error obteniendo usuario."
            }
            res.status(500);
        } else {
          if(body.length > 0) {
            response = body;
          } else {
            response = {
              "msg" : "Usuario no encontrado."
            }
            res.status(404);
          }
        }
        res.send(response);
      });
});

// Petición GET con id en mLab
app.get(URI + 'users/:id',
  function (req, res) {
    console.log("GET /colapi/v1/users/:id");
    console.log(req.params.id);
    var id = req.params.id;
    var queryString = 'q={"id":' + id + '}&';
    var queryStrField = 'f={"_id":0}&';
    var httpClient = requestJSON.createClient(baseMLabURL);
    httpClient.get('ucm?' + queryString + queryStrField + apiKeyMLab,
      function(err, respuestaMLab, body){
        console.log("Respuesta mLab correcta.");
        console.log(body[0]);
        var response = {};
        if(err) {
            response = {
              "msg" : "Error obteniendo usuario."
            }
            res.status(500);
        } else {
          if(body.length > 0) {
            response = body;
          } else {
            response = {
              "msg" : "Usuario no encontrado."
            }
            res.status(404);
          }
        }
        res.send(response);
      });
});


// POST 'users' mLab
app.post(URI + 'users',
  function(req, res){
    var clienteMlab = requestJSON.createClient(baseMLabURL);
    clienteMlab.get('ucm?' + apiKeyMLab,
      function(error, respuestaMLab, body){
        newID = body.length + 1;
        console.log("newID:" + newID);
        var newUser = {
          "id" : +newID,
          "first_name" : req.body.first_name,
          "last_name" : req.body.last_name,
          "email" : req.body.email,
          "password" : req.body.password
        };
        console.log(baseMLabURL + "ucm?" + apiKeyMLab, newUser);
        clienteMlab.post(baseMLabURL + "ucm?" + apiKeyMLab, newUser,
          function(error, respuestaMLab, body){
            res.send(body);
          });
      });
  });

// POST 'users' con ID mLab
app.post(URI + 'users/:id',
  function(req, res){
    console.log("POST /colapi/v1/users/:id");
    console.log(req.params.id);
    var id = req.params.id;
    var queryString = 'q={"id":' + id + '}&';
    var clienteMlab = requestJSON.createClient(baseMLabURL);
      clienteMlab.get('ucm?' + queryString + apiKeyMLab,
        function(err, respuestaMLab, body){
          var response = {};
          if(err) {
              response = {
                "msg" : "Error obteniendo usuario."
              }
              res.status(500);
          } else {
            if(body.length > 0) {
              res.status(404).send({"msg" : "Usuario ya existe."});

            } else {
                var newUser = {
                  "id" : +id,
                  "first_name" : req.body.first_name,
                  "last_name" : req.body.last_name,
                  "email" : req.body.email,
                  "password" : req.body.password
                };
                console.log(baseMLabURL + "ucm?" + apiKeyMLab, newUser);
                clienteMlab.post(baseMLabURL + "ucm?" + apiKeyMLab, newUser,
                  function(error, respuestaMLab, body){
                    res.status(200).send(body);
                  });
                }
          }
      });
});



// PUT 'users' mLab
app.put(URI + 'users/:id',
  function(req, res){
    console.log("PUT /api/v1/users/:id");
    console.log(req.params.id);

    var id = req.params.id;
    var queryString = 'q={"id":' + id + '}&';
    //var queryStrField = 'f={"_id":0}&';
    var clienteMlab = requestJSON.createClient(baseMLabURL);
    clienteMlab.get('user?' + queryString + apiKeyMLab,
      function(err, respuestaMLab, body){
        console.log("Respuesta mLab correcta.");
        var response = {};
        if(err) {
            response = {
              "msg" : "Error obteniendo usuario."
            }
            res.status(500);
        } else {
          if(body.length > 0) {
            var updUser = {
              "id" : +id,
              "first_name" : req.body.first_name,
              "last_name" : req.body.last_name,
              "email" : req.body.email,
              "password" : req.body.password
            };
        //    console.log(baseMLabURL + "user?" + queryString + apiKeyMLab, updUser);
            clienteMlab.put("user?" + queryString + apiKeyMLab, updUser,
              function(error, respuestaMLab, body){
              });
            response = body;
          } else {
            response = {
              "msg" : "Usuario no encontrado."
            }
            res.status(404);
          }
        }
       res.send(response);
      });
});

/* SÓLO A MODO DE EJEMPLO DEJO ESTE MÉTODO PARA REALIZAR DELETE
/* UTILIZANDO EL ID INTERNO DE MLAB (_id).
// Petición DELETE con _id en mLab
app.delete(URI + 'users/:id',
  function (req, res) {
    console.log("DELETE /api-uruguay/v1/users/:id");
    console.log(req.params.id);
    var id = req.params.id;
    var queryString = 'q={"id":' + id + '}&';

    var clienteMlab = requestJSON.createClient(baseMLabURL);
    clienteMlab.get('user?' + queryString + apiKeyMLab,
      function(err, respuestaMLab, body){
        console.log("Respuesta mLab correcta.");

        var response = {};
        if(err) {
            response = {
              "msg" : "Error obteniendo usuario."
            }
            res.status(500);
        } else {
          if(body.length > 0) {
            console.log(baseMLabURL + "user/" + body[0]._id.$oid + '?' + apiKeyMLab);
            clienteMlab.delete("user/" + body[0]._id.$oid + '?' + apiKeyMLab,
              function(error, respuestaMLab, body){
              });
              response = body[0]._id.$oid;


          } else {
            response = {
              "msg" : "Usuario no encontrado."
            }
            res.status(404);
          }
        }
        res.send(response);
      });
});
*/



// Petición DELETE con put y id en mLab
app.delete(URI + 'users/:id',
  function (req, res) {
    console.log("DELETE" + URI + "/users/:id");
    console.log(req.params.id);
    var id = req.params.id;
    var queryString = 'q={"id":' + id + '}&';

    var clienteMlab = requestJSON.createClient(baseMLabURL);
      console.log(baseMLabURL + "ucm?" + queryString + apiKeyMLab);
    clienteMlab.get('ucm?' + queryString + apiKeyMLab,
      function(err, respuestaMLab, body){
        console.log("Respuesta mLab correcta.");
        var response = {};
        if(err) {
            response = {
              "msg" : "Error obteniendo usuario."
            }
            res.status(500);
        } else {
          if(body.length > 0) {
            dltUsr = {};
            console.log(baseMLabURL + "ucm?" + queryString + apiKeyMLab);
            clienteMlab.put("ucm?" + queryString + apiKeyMLab, dltUsr,
              function(error, respuestaMLab, body){
              });
          response = body;

          } else {
            console.log("entró acaaaa");
            response = {
              "msg" : "Usuario no encontrado."
            }
            res.status(404);
          }
        }
        res.send(response);
      });
});


//Method POST login
app.post(URI + 'login',
  function (req, res){
    console.log('POST' + URI + 'login');
    var email = req.body.email;
    var pass = req.body.password;
    var queryStringEmail = 'q={"email":"' + email + '"}&';
    var queryStringpass = 'q={"password":' + pass + '}&';
    var  clienteMlab = requestJSON.createClient(baseMLabURL);

    clienteMlab.get('ucm?'+ queryStringEmail + apiKeyMLab ,
    function(error, respuestaMLab , body){


      var respuesta = body[0];
      console.log(respuesta);

      if(respuesta != undefined){

          if (respuesta.password == pass) {
            console.log("Login Correcto");
            var session = {"logged":true};
            var login = '{"$set":' + JSON.stringify(session) + '}';
            console.log(baseMLabURL+'?q={"id": ' + respuesta.id + '}&' + apiKeyMLab);
            clienteMlab.put('ucm?q={"id": ' + respuesta.id + '}&' + apiKeyMLab, JSON.parse(login),
              function(errorP, respuestaMLabP, bodyP) {
                  res.status(200).send(body[0]);
              });
          }
          else {
            console.log("contraseña incorrecta");
            res.status(401).send({"msg":"Algunos de los datos ingresados no son correctos"});
          }
      } else {
        console.log("Email Incorrecto");
        res.status(401).send({"msg":"Algunos de los datos ingresados no son correctos"});
      }
    });
});


//Method POST logout
app.post(URI + "logout",
  function (req, res){
    console.log('POST' + URI + 'logout');
    var email = req.body.email;
    var queryStringEmail = 'q={"email":"' + email + '"}&';
    var  clienteMlab = requestJSON.createClient(baseMLabURL);
    clienteMlab.get('ucm?'+ queryStringEmail + apiKeyMLab ,
    function(error, respuestaMLab, body) {
      console.log("entro al get");
      var respuesta = body[0];
      console.log(respuesta);
      if(respuesta != undefined){
            console.log("logout Correcto");
            var session = {"logged":true};
            var logout = '{"$unset":' + JSON.stringify(session) + '}';
            console.log(logout);
            clienteMlab.put('ucm?q={"id": ' + respuesta.id + '}&' + apiKeyMLab, JSON.parse(logout),
              function(errorP, respuestaMLabP, bodyP) {
                res.send(body[0]);
              });
      } else {
        console.log("Error en logout");
        res.send({"msg": "Error en logout"});
      }
    });
});

// Método para cambio de contraseña
// PUT 'password'
app.put(URI + 'userpassword/:id',
  function(req, res){
    console.log("PUT /api-uruguay/v1/userpassword/:id");
    console.log(req.params.id);

    var id = req.params.id;
    var password = req.body.password;
    var newPassword = req.body.new_password;

    var queryString = 'q={"id":' + id + '}&';

    var clienteMlab = requestJSON.createClient(baseMLabURL);
    clienteMlab.get('ucm?' + queryString + apiKeyMLab,
      function(err, respuestaMLab, body){
        console.log("Respuesta mLab correcta.");
        console.log(body[0]);
        var response = {};
        if(err) {
            response = {
              "msg" : "Error obteniendo usuario."
            }
            res.status(500);
        } else {
          if(body.length > 0) {

              if(body[0].password==password) {
                  var updUser = body[0];
                  updUser.password = newPassword;

                  clienteMlab.put("ucm?" + queryString + apiKeyMLab, updUser,
                    function(error, respuestaMLab, body){
                    });
                    response = body;
                } else {
                  console.log("password actual no coincide");
                  response = {
                    "msg" : "Alguno de los datos ingresados es incorrecto."
                  }
                }
          } else {
            response = {
              "msg" : "Usuario no encontrado."
            }
            res.status(404);
          }
        }
       res.send(response);
      });
});
